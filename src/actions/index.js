  export const GET_CITY_NAMES = "GET_CITY_NAMES";
  export const GET_CITY_NAMES_SUCCESS = "GET_CITY_NAMES_SUCCESS";
  export const GET_CITY_NAMES_FAIL = "GET_CITY_NAMES_FAIL";

  export const GET_CITY_DETAILS = "GET_CITY_DETAILS";
  export const GET_CITY_DETAILS_SUCCESS = "GET_CITY_DETAILS_SUCCESS";
  export const GET_CITY_DETAILS_FAIL = "GET_CITY_DETAILSS_FAIL";

  export const GET_WEATHER_DETAILS = "GET_WEATHER_DETAILS";
  export const GET_WEATHER_DETAILS_SUCCESS = "GET_WEATHER_DETAILS_SUCCESS";
  export const GET_WEATHER_DETAILS_FAIL = "GET_WEATHER_DETAILSS_FAIL";

  export const CLEAR_CITY_LIST = "CLEAR_CITY_LIST";
  export const HANDLE_DELETE_ROW = "HANDLE_DELETE_ROW";
  export const HANDLE_UNIT_CHANGE = "HANDLE_UNIT_CHANGE";

  export const getCityNames= searchTerm => ({
    type: 'GET_CITY_NAMES',
    searchTerm
  });

  export const getCityNamesSuccess= (payload, st) => ({
    type: 'GET_CITY_NAMES_SUCCESS',
    payload,
    searchTerm : st
  });

  export const getCityNamesFail= error => ({
    type: 'GET_CITY_NAMES_FAIL',
    error
  });

  export const getCityDetails = cityObject => ({
    type: 'GET_CITY_DETAILS',
    cityObject
  });

  export const getCityDetailsSuccess= (payload, obj) => ({
    type: 'GET_CITY_DETAILS_SUCCESS',
    payload,
    cityObject : obj
  });

  export const getCityDetailsFail= error => ({
    type: 'GET_CITY_DETAILS_FAIL',
    error
  });

  export const getWeatherDetails = cityObject => ({
    type: 'GET_WEATHER_DETAILS',
    cityObject
  });

  export const getWeatherDetailsSuccess= (payload,obj) => ({
    type: 'GET_WEATHER_DETAILS_SUCCESS',
    payload,
    cityObject : obj
  });

  export const getWeatherDetailsFail= error => ({
    type: 'GET_WEATHER_DETAILS_FAIL',
    error
  });

  export const clearCityList = () => ({
    type: 'CLEAR_CITY_LIST'
  });

  export const handleDeleteRow = (placeId) => ({
    type: 'HANDLE_DELETE_ROW',
    placeId
  });

  export const handleUnitChange = (unit,placeId) => ({
    type: 'HANDLE_UNIT_CHANGE',
    placeId,
    unit
  });

  
