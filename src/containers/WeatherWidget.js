import React from "react";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import {
    getCityNames,
    getCityDetails,
    getWeatherDetails,
    clearCityList,
    handleDeleteRow,
    handleUnitChange
  } from "../actions";
import WidgetRow from '../components/WidgetRow';
import AddPlace from '../components/AddPlace';
import "../sagas";
import { isEqual } from "lodash";

class WeatherWidget extends React.Component {
    state ={
        cityDetails:{},
        cityNames:[],
        renderedCities: []
    }

    componentWillReceiveProps(nextProps) {
        const {
            cityDetails,
            cityNames,
            renderedCities
        } = nextProps;
        const {
            getWeatherDetails
        } = this.props;
        const updatedState = {};
        if(!isEqual(cityNames, this.state.cityNames))
        {
            updatedState.cityNames = cityNames;
        }
        if(!isEqual(cityDetails, this.state.cityDetails))
        {
            updatedState.cityDetails = cityDetails;
            getWeatherDetails(cityDetails);
        }
        if(!isEqual(renderedCities, this.state.renderedCities))
        {  
            updatedState.renderedCities = renderedCities;
        }
        if (Object.keys(updatedState).length) {
            this.setState(updatedState);
        }
    }

    handleSelectCity = (cityObject, unit) => () => {
      const { getCityDetails } = this.props;
      cityObject.unit=unit;
      getCityDetails(cityObject);
    }

    handleDeleteRow = (placeId) => () => {
        const { handleDeleteRow } = this.props;
        handleDeleteRow(placeId);
    }

    handleUnitChange = (unit, cityDetails) => () =>{
        const { handleUnitChange } = this.props;
        if(cityDetails.unit !== unit)
          handleUnitChange(unit, cityDetails.placeId);
    }

   render() {  
    const {
        getCityNames,
        getCityDetails,
        getWeatherDetails,
        clearCityList, 
        loading
    } = this.props;
    const {
        cityNames,
        renderedCities
    } = this.state;
    const rows = renderedCities.length > 0 ? renderedCities.map((cityObj) => {
        let classN= "bg-ina";
        const weather = cityObj.weather.toLowerCase();
        if(weather.indexOf('cloud')>-1)
           classN = "bg-cloudy";
        else if(weather.indexOf('snow')>-1)
            classN = "bg-snowy";
        else if(weather.indexOf('rain')>-1)
            classN = "bg-rainy";
        else if(weather.indexOf('wind')>-1)
            classN = "bg-windy";
        else if(weather.indexOf('sun')>-1)
            classN = "bg-sunny";
        else if(weather.indexOf('clear')>-1)
            classN = "bg-clear";
        else if(weather.indexOf('haze')>-1)
            classN = "bg-haze"; 
        else if(weather.indexOf('mist')>-1)
            classN = "bg-mist";    
         else    
            classN = "bg-ina";
        cityObj.classN= classN;
            return (
                <WidgetRow key={cityObj.placeId} cityDetails={cityObj} handleUnitChange= {this.handleUnitChange} handleDeleteRow={this.handleDeleteRow}/>
            ) 
        }) : null;
    
    return (
        <div className="d-t m-auto place-box">
            <div className="b-1x b-r-4x">
                <div>
                    <AddPlace 
                        getCityNames={getCityNames}
                        getCityDetails={getCityDetails}
                        getWeatherDetails={getWeatherDetails}
                        cityNames={cityNames}
                        handleSelectCity={this.handleSelectCity}
                        clearCityList={clearCityList}
                    />
                </div>
                <div>
                    {rows!==null ?
                    <div className="m-1-2 mi-w-300x b-1x">
                        {rows}
                    </div>  : null
                    }
                </div>
            </div>
            { loading ? (
                <div className="loader-container">
                    <div className="loader"></div>
                </div>) : null}
        </div>   
    );
}
}

WeatherWidget.propTypes = {
    getCityNames: PropTypes.func.isRequired,
    getCityDetails: PropTypes.func.isRequired,
    getWeatherDetails: PropTypes.func.isRequired,
    clearCityList: PropTypes.func.isRequired,
    handleDeleteRow: PropTypes.func.isRequired,
    handleUnitChange: PropTypes.func.isRequired,
    cityNames: PropTypes.array,
    cityDetails: PropTypes.object,
    weatherDetails: PropTypes.object,
    renderedCities: PropTypes.array,
    loading: PropTypes.bool
};

AddPlace.defaultProps = {
    cityNames: [],
    cityDetails: {},
    weatherDetails : {},
    renderedCities :[],
    loading: false
  };

const mapStateToProps = (state) => ({
    cityNames: state.cityNames,
    cityDetails: state.cityDetails,
    weatherDetails: state.weatherDetails,
    renderedCities: state.renderedCities,
    loading: state.loading
});

export default connect(
    mapStateToProps,
    {   getCityNames,
        getCityDetails,
        getWeatherDetails,
        clearCityList,
        handleDeleteRow,
        handleUnitChange
    }
)(WeatherWidget);
