import React from 'react';
import createSagaMiddleware from 'redux-saga';
import { render } from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import {  BrowserRouter as Router } from "react-router-dom";
import reducer from './reducers';
import App from './app';
import './sagas';
import sagasManager from "./sagasManager";
const sagaMiddleware = createSagaMiddleware();
const reduxDevTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() || compose;

// creating a redux store with reducer and middleware
let store = createStore(
  reducer,
  compose(applyMiddleware(sagaMiddleware), reduxDevTools)
);

sagaMiddleware.run(sagasManager.getRootSaga());
render(
   <Provider store={store}>
    <Router>
      <App />
     </Router>
   </Provider>,
document.getElementById('root'),
);