import React from "react";
import { shallow, mount, render } from 'enzyme';

import WidgetRow from '../components/WidgetRow';


describe('Weather component', () => {
  test('should shallow correctly', () => {
      expect(shallow(
        <WidgetRow />
      )).toMatchSnapshot() 
  })
  test('should mount correctly', () => {
      expect(mount(
        <WidgetRow />
      )).toMatchSnapshot() 
  })
  test('should render correctly', () => {
      expect(render(
        <WidgetRow />
      )).toMatchSnapshot() 
  })
})