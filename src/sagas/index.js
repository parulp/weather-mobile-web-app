
  import { put, call, all, takeLatest } from "redux-saga/effects";
  import * as endpoints from "../helpers/api/endpoints";
  import {
   GET_CITY_NAMES,
   GET_CITY_DETAILS,
   GET_WEATHER_DETAILS,
   getCityNamesSuccess,
   getCityNamesFail,
   getCityDetailsSuccess,
   getCityDetailsFail,
   getWeatherDetailsSuccess,
   getWeatherDetailsFail,
 } from "../actions";

  import sagasManager from "../sagasManager";

  function* getCityNames(payload) {
    try {
      const apiData = yield call(endpoints.getCityNames, payload.searchTerm);
      yield put(getCityNamesSuccess(apiData, payload.searchTerm));
    } catch (error) {
      console.log(e);
      yield put(getCityNamesFail(e));
    }
  }

  function* getCityDetails(payload) {
    try {
      const apiData = yield call(endpoints.getCityDetails, payload.cityObject.placeId);
      yield put(getCityDetailsSuccess(apiData, payload.cityObject));
    } catch (error) {
      console.log(e);
      yield put(getCityDetailsFail(e));
    }
  }

  function* getWeatherDetails(payload) {
    try {
      const apiData = yield call(endpoints.getWeatherDetails, payload.cityObject);
      yield put(getWeatherDetailsSuccess(apiData, payload.cityObject));
    } catch (error) {
      console.log(e);
      yield put(getWeatherDetailsFail(e));
    }
  }

  sagasManager.addSagaToRoot(function* watcher() {
    yield all([
      takeLatest(GET_CITY_NAMES, getCityNames),
      takeLatest(GET_CITY_DETAILS, getCityDetails),
      takeLatest(GET_WEATHER_DETAILS, getWeatherDetails)
    ]);
  });