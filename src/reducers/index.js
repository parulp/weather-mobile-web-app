import {
   GET_CITY_NAMES,
  GET_CITY_NAMES_SUCCESS,
  GET_CITY_NAMES_FAIL,
  GET_CITY_DETAILS,
  GET_CITY_DETAILS_SUCCESS,
  GET_CITY_DETAILS_FAIL,
  GET_WEATHER_DETAILS,
  GET_WEATHER_DETAILS_SUCCESS,
  GET_WEATHER_DETAILS_FAIL,
  CLEAR_CITY_LIST,
  HANDLE_DELETE_ROW,
  HANDLE_UNIT_CHANGE
 } from "../actions";
 

export const initialState = {
    error: null,
    cityNames: [],
    cityDetails: {},
    weatherDetails: {},
    renderedCities:[],
    loading: false,
  };

const reducer = (state = initialState, action) => {
    switch (action.type) {
      case CLEAR_CITY_LIST:
         return { ...state, error: null, cityNames: [] };
      case HANDLE_DELETE_ROW:{
         const placeId= action.placeId;
         const renderedCities= state.renderedCities && state.renderedCities.length>0 ? Object.assign([], state.renderedCities) : [];
         if (renderedCities.length > 0)
         { 
            let index = -1;
            renderedCities.forEach((obj, i) => {
               if(obj.placeId === placeId)
               {
                  index= i;
                  return;
               }
            });
            renderedCities.splice(index, 1);
         }
         return { ...state, error: null, renderedCities }
      };
      case HANDLE_UNIT_CHANGE:{
         const placeId= action.placeId;
         const unit = action.unit;
         const renderedCities= state.renderedCities && state.renderedCities.length>0 ? Object.assign([], state.renderedCities) : [];
         if (renderedCities.length > 0)
         { 
            renderedCities.forEach((obj) => {
               if(obj.placeId === placeId)
               {
                  if(obj.unit === "C" && unit==="F")
                  {
                     obj.temperature = ((obj.temperature * 9/5) + 32).toFixed(2);;
                     obj.unit = "F";
                  }
                  else if(obj.unit === "F" && unit==="C")
                  {
                     obj.temperature = ((obj.temperature - 32) * 5/9).toFixed(2);;
                     obj.unit = "C";
                  }
               }
            });
         }
         return { ...state, error: null, renderedCities }
      };
      case GET_CITY_NAMES:
         return { ...state, error: null, loading: true };
       case GET_CITY_DETAILS:
          return { ...state, error: null, loading: true };
       case GET_WEATHER_DETAILS:
          return { ...state, loading: true, error: null };
       case GET_CITY_NAMES_SUCCESS: {
           const data = action.payload;
           const searchTerm = (action.searchTerm).toUpperCase();
           let cities =[];
           if(data && data.predictions)
           {
              data.predictions.forEach((cityObject)=>{

                 if(cityObject.description &&
                  cityObject.description.length>0 &&
                  cities.indexOf(cityObject.description)<0 &&
                  ((cityObject.description).toUpperCase()).indexOf(searchTerm)>-1
                  && cityObject.place_id)
                  {
                     cities.push({name: cityObject.description, placeId: cityObject.place_id})
                  }
              })
           }
           return { ...state, error: null, cityNames: cities, loading: false};
       }
       case GET_CITY_NAMES_FAIL: {
          const error = action.payload;
          return { ...state, error, loading: false };
       }
       case GET_CITY_DETAILS_SUCCESS: {
         const data = action.payload;
         const cityDetails = {... action.cityObject, lat:'NA', lng:'NA'};
         const renderedCities = state.renderedCities && state.renderedCities.length>0 ? Object.assign([], state.renderedCities) : [];
         let found= false;
         if(data && data.result && data.result.geometry && data.result.geometry.location && data.result.geometry.location.lat && data.result.geometry.location.lng)
         { 
            cityDetails.lat= data.result.geometry.location.lat;
            cityDetails.lng= data.result.geometry.location.lng;
            renderedCities.forEach((obj) => {
               if(obj.placeId === cityDetails.placeId )
               {
                  found= true;
                  obj = {...cityDetails,weather:"NA"};
                  return;
               }
            })
            if(!found)
            {
               cityDetails.weather="NA";
             renderedCities.push(cityDetails);
            }
         }
         return { ...state, error: null, renderedCities, cityDetails, loading: false};
     }
     case GET_CITY_DETAILS_FAIL: {
        const error = action.payload;
        return { ...state, error, loading: false };
     }
     case GET_WEATHER_DETAILS_SUCCESS: {
        let temperature = "NA";
        let weather = "NA";
        const cityObject = action.cityObject;
        const renderedCities = Object.assign([], state.renderedCities)
      if (action.payload && action.payload.main)
      {
        temperature = action.payload.main.temp;
      }
      if (action.payload && action.payload.weather && action.payload.weather.length>0)
      {
        weather = action.payload.weather[0].description;
      }
      
      renderedCities.forEach((obj)=>{
        if(obj.placeId== cityObject.placeId){
         temperature = parseFloat(temperature);
         if (obj.unit === "F") {
            temperature = ((temperature - 273.15)*(9/5) + 32).toFixed(2);
         } else {
            temperature = (temperature - 273.15).toFixed(2);
         }
         obj.temperature=temperature;
         obj.weather = weather;
        }
      });
      return { ...state, error: null, weatherDetails: {temperature, weather}, renderedCities, loading: false};
  }
  case GET_WEATHER_DETAILS_FAIL: {
     const error = action.payload;
     return { ...state, error, loading: false };
  }
       default:
          return state;
    }
  };
  export default reducer;
