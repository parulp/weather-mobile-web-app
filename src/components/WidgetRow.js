import React from "react";
import PropTypes from 'prop-types';

const crossIcon = require("../assets/cross.png");


const WidgetRow = (props) => {
    const { cityDetails, handleDeleteRow, handleUnitChange } = props;
    const classname = cityDetails.classN;
    return (
        <div className={classname}>
        <div className="d-f fs-20x p-30x row">
            <div className="name-div">
                {cityDetails.name}
            </div>
            <div className="data-div">
            <div className="temp-div">
                <div className="c-p">
                    <span className={cityDetails.unit === 'C' ? 'selected' : ''} onClick={handleUnitChange('C',cityDetails)}>°C</span>
                    &nbsp;/&nbsp;
                    <span className={cityDetails.unit === 'F' ? 'selected' : ''} onClick={handleUnitChange('F', cityDetails)}>°F</span>
                </div>
            </div>
            <div className="unit-div">
                {cityDetails.temperature}°{cityDetails.unit}
            </div>
            <div className="cross-div" onClick={handleDeleteRow(cityDetails.placeId)}>
                <img src={crossIcon} className="ht-23x" />
            </div>
            </div>
        </div>
        </div>
    );

}

WidgetRow.propTypes = {
    cityDetails: PropTypes.object,
    handleDeleteRow: PropTypes.func.isRequired,
};

export default WidgetRow;