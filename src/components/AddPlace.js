import React from 'react';
import PropTypes from 'prop-types';
import { isEqual } from "lodash";

const addIcon = require("../assets/add.png");
const searchIcon = require("../assets/search.png");


 class AddPlace extends React.Component {
     state = {
        showAddBar: false, 
        unit: "C", 
        searchValue: "",
        cityNames: [],
        selectedCity: {}
     };
     
     componentWillReceiveProps(nextProps) {
        const {
            cityNames
        } = nextProps;
        const { showAddBar,unit } = this.state;
        const { handleSelectCity } = this.props;
        if(!isEqual(cityNames, this.state.cityNames))
        {
            this.setState({cityNames});
            if(!showAddBar && cityNames.length>0){
                handleSelectCity(cityNames[0], unit)();
            }
        }
    }

    handleShowAddBar = () => {
      this.setState({showAddBar: true});
    }
  
    handleSearchChange = (e) => {
        const { getCityNames } = this.props;
        this.setState({
            searchValue: e.target.value 
        })
        getCityNames(e.target.value);       
    }

    handleSearch = () => {
      const {searchValue} = this.state;
      const { getCityNames } = this.props;
      getCityNames(searchValue);
    }

    handleUnitChange = (unit) => () => {   
       this.setState({unit}); 
    }

    handleSelectCity = (cityObj) => () => {
       const { clearCityList } = this.props;
       this.setState({searchValue : cityObj.name, selectedCity:cityObj});
       clearCityList();
    }

    handleAddPlace = () => {
        const { unit, selectedCity, searchValue} = this.state;
        const { handleSelectCity, clearCityList } = this.props;
        if(selectedCity && selectedCity.placeId)
        {
            handleSelectCity(selectedCity, unit)();
        } else if(searchValue.length>0){
            getCityNames(searchValue);
        }
        this.setState({
            selectedCity: {},
            searchValue: "",
            showAddBar: false
        });
        clearCityList();
    }

  
    render() {  
        const { showAddBar, cityNames, searchValue, unit } = this.state;
        const suggestions = cityNames.length > 0 ? 
            cityNames.map((cityObj) => {
                return (<div key={cityObj.placeId} className= "p-4x-8x c-p" onClick={this.handleSelectCity(cityObj)}>{cityObj.name}</div>) 
            }) : null;
        return (
            <div className="p-2 mi-w-300x">
                {showAddBar ?
                    (<div>
                        <div>
                            <div className= "d-t m-auto">
                                <div className="d-f">
                                    <input type="text" className="p-8x t-n b-1x fs-15x b-t-l-5x b-b-l-5x input-box" placeholder="Enter city name" value={searchValue} onChange={this.handleSearchChange}/>
                                    <div className="ht-50x b-1x b-t-r-5x b-b-r-5x w-50x c-p">
                                        <img src={searchIcon} className="ht-80 p-10" onClick={this.handleSearch}/>
                                    </div>
                                </div>
                            </div>
                            <div className= "d-t m-auto b-1x m-b-20x sugg-box">
                                {suggestions}
                            </div>
                            <div>
                                <div className="d-t m-auto">
                                <div className="d-f unit-container"> 
                                        <div className="unit-box"> Units </div>
                                        <div className="c-p">
                                            <span className={unit === 'C'? 'selected': ''} onClick={this.handleUnitChange('C')}>°C</span>&nbsp;/&nbsp;
                                            <span className={unit === 'F'? 'selected': ''} onClick={this.handleUnitChange('F')}>°F</span> 
                                        </div>                               
                                </div>
                                </div>
                            </div> 
                            <div className="d-t m-auto c-p">   
                            <div className="d-f b-1x p-5x w-60x b-r-4x" onClick={this.handleAddPlace}>
                                <img src={addIcon} className="ht-20x"/>
                                <span>ADD</span>
                            </div>
                            </div>
                        </div>
                    </div>):
                    (<div onClick={this.handleShowAddBar} className="b-1x c-p">
                        <img src={addIcon} className="ht-50x d-b m-auto p-1-0"/>
                    </div>)
                }       
            </div>
        );
    }
}

AddPlace.propTypes = {
    getCityNames: PropTypes.func.isRequired,
    clearCityList: PropTypes.func.isRequired,
    handleSelectCity: PropTypes.func.isRequired,
    cityNames: PropTypes.array
};

AddPlace.defaultProps = {
    cityNames: [],
};

export default AddPlace;