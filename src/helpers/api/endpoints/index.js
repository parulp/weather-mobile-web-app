
const weatherApiKey = "0cc1938997ad472774921abcfdfc8dd5"; 
// my key const googleMapApiKey = "AIzaSyBtkcKFJ1qOU1D3Dzh6wiR3-O0tbROBW04";
const googleMapApiKey = "AIzaSyDrVNKZshUspEprFsNnQD-sos6tvgFdijg";
const weatherBaseUrl = "https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/weather";
const mapsSuggestionBaseUrl = "https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/place/autocomplete/json";
const mapsCityDetailsBaseUrl= "https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/place/details/json";

const headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": '*',
    "Access-Control-Allow-Methods": "GET, OPTIONS, HEAD",
    "Access-Control-Allow-Headers": "Origin, Methods, Content-Type"
  };

// GET API FOR CITY NAME SUGGESTION
export function getCityNames(searchterm) {
  const url = `${mapsSuggestionBaseUrl}?input=${searchterm}&types=(cities)&key=${googleMapApiKey}`;
  return fetch(url, {
    method: "GET",
    headers
  }).then(response => response.json());   
}

// GET API FOR CITY DETAILS
export function getCityDetails(placeId) {
  const url = `${mapsCityDetailsBaseUrl}?placeid=${placeId}&fields=name,geometry&key=${googleMapApiKey}`;
  return fetch(url, {
    method: "GET",
    headers
  }).then(response => response.json());   
}

// GET API FOR CITY DETAILS
export function getWeatherDetails(location) {
  const url = `${weatherBaseUrl}?lat=${location.lat}&lon=${location.lng}&appid=${weatherApiKey}`;
  return fetch(url, {
    method: "GET",
    headers
  }).then(response => response.json());   
}
